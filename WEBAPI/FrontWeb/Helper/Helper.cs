﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontWeb.Helper
{
    public class PersonaAPI
    {
        public HttpClient Initial()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:46013/");
            return client;
        }

        public HttpClient External()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://api.toka.com.mx/candidato/");
            return client;
        }
    }
}
