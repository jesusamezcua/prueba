export class PersonaModel {
    IdPersonaFisica:number = 0;
    Nombre:string = '';
    ApellidoPaterno:string = '';
    ApellidoMaterno:string = '';
    RFC:string = '';
    FechaNacimiento: Date= new Date();
    UsuarioAgrega:number = 0;

    constructor(jsnForm: object) {
        this.IdPersonaFisica = 0;
        this.Nombre = jsnForm['Nombre'];
        this.ApellidoPaterno = jsnForm['ApellidoPaterno'];
        this.ApellidoMaterno = jsnForm['ApellidoMaterno'];
        this.RFC = jsnForm['RFC'];
        this.FechaNacimiento = jsnForm['FechaNacimiento'];
        this.UsuarioAgrega = jsnForm['UsuarioAgrega'];
    }
}
