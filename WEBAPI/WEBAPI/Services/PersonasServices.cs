﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WEBAPI.Common;
using WEBAPI.IServices;
using WEBAPI.Models;

namespace WEBAPI.Services
{
    public class PersonasServices : IPersonaService
    {
        Persona _oPersona = new Persona();
        Resp _oResp = new Resp();
        List<Persona> _oPersonas = new List<Persona>();
        public Resp Delete(int IdPersonaFisica)
        {
            _oResp = new Resp();
            try
            {
                using (IDbConnection con = new SqlConnection(Global.ConnectionString))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var oResp = con.Query<Resp>("sp_EliminarPersonaFisica",
                        this.SetParametersDelete(IdPersonaFisica),
                        commandType: CommandType.StoredProcedure);

                    _oResp = oResp.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                _oResp.vchMensaje = ex.Message;
            }
            return _oResp;
        }

        public Persona Get(int IdPersonaFisica)
        {
            _oPersona = new Persona();
            using (IDbConnection con = new SqlConnection(Global.ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                var oPersonas = con.Query<Persona>("SELECT * FROM Tb_PersonasFisicas WHERE IdPersonaFisica = " + IdPersonaFisica).ToList();

                if (oPersonas != null && oPersonas.Count() > 0)
                {
                    _oPersona = oPersonas.SingleOrDefault();
                }
            }
            return _oPersona;
        }

        public List<Persona> Gets()
        {
            _oPersonas = new List<Persona>();
            using (IDbConnection con = new SqlConnection(Global.ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                var oPersonas = con.Query<Persona>("SELECT * FROM Tb_PersonasFisicas WHERE Activo = 1").ToList();

                if (oPersonas != null && oPersonas.Count() > 0)
                {
                    _oPersonas = oPersonas;
                }
            }
            return _oPersonas;
        }

        public Resp Save(Persona oPersona)
        {
            _oResp = new Resp();
            try
            {
                using (IDbConnection con = new SqlConnection(Global.ConnectionString))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var oResp = con.Query<Resp>("sp_AgregarPersonaFisica",
                        this.SetParameters(oPersona),
                        commandType: CommandType.StoredProcedure);
                    _oResp = oResp.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _oResp.vchMensaje = ex.Message;
            }
            return _oResp;
        }

        public Resp Update(Persona oPersona, int id)
        {
            _oResp = new Resp();
            try
            {
                using (IDbConnection con = new SqlConnection(Global.ConnectionString))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var oResp = con.Query<Resp>("sp_ActualizarPersonaFisica",
                        this.SetParametersUpdate(oPersona, id),
                        commandType: CommandType.StoredProcedure);
                    _oResp = oResp.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _oResp.vchMensaje = ex.Message;
            }
            return _oResp;
        }

        private DynamicParameters SetParameters(Persona oPersona)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Nombre", oPersona.Nombre);
            parameters.Add("@ApellidoPaterno", oPersona.ApellidoPaterno);
            parameters.Add("@ApellidoMaterno", oPersona.ApellidoMaterno);
            parameters.Add("@RFC", oPersona.RFC);
            parameters.Add("@FechaNacimiento", oPersona.FechaNacimiento);
            parameters.Add("@UsuarioAgrega", oPersona.UsuarioAgrega);
            return parameters;
        }

        private DynamicParameters SetParametersUpdate(Persona oPersona, int id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@IdPersonaFisica", id);
            parameters.Add("@Nombre", oPersona.Nombre);
            parameters.Add("@ApellidoPaterno", oPersona.ApellidoPaterno);
            parameters.Add("@ApellidoMaterno", oPersona.ApellidoMaterno);
            parameters.Add("@RFC", oPersona.RFC);
            parameters.Add("@FechaNacimiento", oPersona.FechaNacimiento);
            parameters.Add("@UsuarioAgrega", oPersona.UsuarioAgrega);
            return parameters;
        }

        private DynamicParameters SetParametersDelete(int id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@IdPersonaFisica", id);
            return parameters;
        }
    }
}
