import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientesModel } from 'src/app/Modelos/Personas/clientes-model';
import { ReporteService } from 'src/app/Servicios/reporte.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

@Component({
  selector: 'app-generador',
  templateUrl: './generador.component.html',
  styleUrls: ['./generador.component.css']
})
export class GeneradorComponent implements OnInit {

  columnas: string[] = [
    'IdCliente', 'FechaRegistroEmpresa', 'RazonSocial',
    'RFC', 'Sucursal', 'IdEmpleado',
    'Nombre', 'Paterno', 'Materno', 'IdViaje'];
  listaCiente: ClientesModel[] = [];
  dataSource = null;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dataservice: ReporteService) { }

  ngOnInit() {
    this.cargarReporte();
  }

  async cargarReporte(){
    try {
      let token = await this.dataservice.getToken();
      if(token.Data){
        let res = await this.dataservice.getClientes(token.Data);
        if(res['Data']){
          this.listaCiente = res['Data'];
          console.log(this.listaCiente)
          this.dataSource = new MatTableDataSource<ClientesModel>(this.listaCiente);
          this.dataSource.paginator = this.paginator;
        }else{
          alert("Ocurrio un error !!!");
        }
      }else{
        alert("Ocurrio un error !!!");
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  downloadExcel(){
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Employee Data");
    let headerRow = worksheet.addRow(this.columnas);
    for (let x1 of this.listaCiente) {
      let x2 = Object.keys(x1);
      let temp = []
      for (let y of x2) {
        temp.push(x1[y])
      }
      worksheet.addRow(temp)
    }
    let fname="Reporte"
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, fname + '-' + new Date().valueOf() + '.xlsx');
    });
  }

}
