﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontWeb.Models
{
    public class RegistroModel
    {
        public string Correo { get; set; }
        public string Contrasena { get; set; }

        public string ContrasenaConfirm { get; set; }
    }
}
