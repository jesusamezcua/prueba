import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PersonaModel } from '../Modelos/Personas/persona-model';
import { Resp } from '../Modelos/Respuestas/resp';

@Injectable({
  providedIn: 'root'
})
export class PersonasServicioService {
  readonly strUrl = environment.strUrl;
  constructor(private http: HttpClient) {
  }

  public getPersonas(): Promise<PersonaModel[]> {
    return this.http.get<PersonaModel[]>(this.strUrl+'/Personas').toPromise();
  }

  public deletePersona(idPersona: number): Promise<Resp> {
    return this.http.delete<Resp>(this.strUrl+'/Personas/'+idPersona).toPromise();
  }

  public agregarPersona(Persona: PersonaModel): Promise<Resp> {
    return this.http.post<Resp>(this.strUrl+'/Personas', Persona).toPromise();
  }

  public getPersona(idPersona: number): Promise<PersonaModel> {
    return this.http.get<PersonaModel>(this.strUrl+'/Personas/'+idPersona).toPromise();
  }

  public modificarPersona(Persona: PersonaModel, idPersona: number): Promise<Resp> {
    return this.http.put<Resp>(this.strUrl+'/Personas/'+idPersona, Persona).toPromise();
  }
}
