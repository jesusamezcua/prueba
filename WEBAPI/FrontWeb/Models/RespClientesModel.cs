﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontWeb.Models
{
    public class RespClientesModel
    {
        public List<ClienteModel> Data { get; set; }
    }
}
