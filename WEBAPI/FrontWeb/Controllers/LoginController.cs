﻿using FrontWeb.Helper;
using FrontWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace FrontWeb.Controllers
{
    public class LoginController : Controller
    {
        PersonaAPI _api = new PersonaAPI();

        // GET: LoginController
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel login)
        {
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.PutAsJsonAsync($"api/Login/", login);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                RespLoginModel Respuesta = JsonConvert.DeserializeObject<RespLoginModel>(result);
                if (Respuesta.intStatus == 200)
                {
                    var claims = new List<Claim>
                    {
                        new Claim("id", Respuesta.IdUsuario.ToString())
                    };
                    var claimsIdentity = new ClaimsIdentity(claims, "Login");
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                    return RedirectToAction("Index","Home");
                }
                else
                {
                    TempData["Mensaje"] = Respuesta.vchMensaje;
                }
            }
            return View();
        }

        public ActionResult Registro()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Registro(RegistroModel registro)
        {
            if (registro.Contrasena == registro.ContrasenaConfirm)
            {
                HttpClient client = _api.Initial();
                HttpResponseMessage res = await client.PostAsJsonAsync("api/Login", registro);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    RespLoginModel Respuesta = JsonConvert.DeserializeObject<RespLoginModel>(result);
                    if (Respuesta.intStatus == 200)
                    {
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        TempData["Mensaje"] = Respuesta.vchMensaje;
                    }
                }
            }
            else
            {
                TempData["Mensaje"] = "Las contraseñas no coinciden";
            }

            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Login");

        }
    }
}
