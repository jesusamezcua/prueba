﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontWeb.Models
{
    public class RespLoginModel
    {
        public int intStatus { get; set; }
        public string vchMensaje { get; set; }
        public int IdUsuario { get; set; }
    }
}
