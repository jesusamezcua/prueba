import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AgregarPersonaComponent } from '../agregar-persona/agregar-persona.component';
import { HttpClientModule } from '@angular/common/http';
import { CRUDPersonaComponent } from '../crudpersona/crudpersona.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';

const routes: Routes = [
  { path: 'agregarPersona', component: AgregarPersonaComponent },
  { path: 'agregarPersona/:idPersonaFisica', component: AgregarPersonaComponent },
  { path: '', component: CRUDPersonaComponent }
]

@NgModule({
  declarations: [AgregarPersonaComponent,
    CRUDPersonaComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [MatButtonModule, MatCheckboxModule,MatDatepickerModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class PersonaModule { }
