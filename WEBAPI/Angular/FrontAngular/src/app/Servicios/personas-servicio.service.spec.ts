import { TestBed } from '@angular/core/testing';

import { PersonasServicioService } from './personas-servicio.service';

describe('PersonasServicioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonasServicioService = TestBed.get(PersonasServicioService);
    expect(service).toBeTruthy();
  });
});
