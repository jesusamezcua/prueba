﻿using ClosedXML.Excel;
using FrontWeb.Helper;
using FrontWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace FrontWeb.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        PersonaAPI _api = new PersonaAPI();

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            List<PersonaData> persona = new List<PersonaData>();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/Personas");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                persona = JsonConvert.DeserializeObject<List<PersonaData>>(result);
            }
            return View(persona);
        }

        public async Task<IActionResult> Details(int Id)
        {
            var persona = new PersonaData();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Personas/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                persona = JsonConvert.DeserializeObject<PersonaData>(result);
            }
            return View(persona);
        }

        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult create(PersonaData persona)
        {
            HttpClient client = _api.Initial();
            //HTTP POST
            var postTask = client.PostAsJsonAsync<PersonaData>("api/Personas", persona);
            postTask.Wait();
            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var res = result.Content.ReadAsStringAsync().Result;
                RespPersonaModel Respuesta = JsonConvert.DeserializeObject<RespPersonaModel>(res);
                if (Respuesta.intStatus == 200)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Mensaje"] = Respuesta.vchMensaje;
                }
            }
            return View();
        }

        public async Task<IActionResult> Delete(int id)
        {
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.DeleteAsync($"api/Personas/{id}");
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var persona = new PersonaData();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync($"api/Personas/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                persona = JsonConvert.DeserializeObject<PersonaData>(result);
            }
            return View(persona);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, PersonaData persona)
        {
            HttpClient client = _api.Initial();

            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/Personas/{id}", persona);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Reporte(int? page)
        {
            List<ClienteModel> Respuesta2 = new List<ClienteModel>();
            object user = new TokenModel();
            HttpClient client = _api.External();
            HttpResponseMessage res = await client.PostAsJsonAsync($"api/login/authenticate/", user);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                TokenModel Respuesta = JsonConvert.DeserializeObject<TokenModel>(result);
                client.DefaultRequestHeaders.Add("Authorization", Respuesta.Data);
                HttpResponseMessage res2 = await client.GetAsync($"api/customers");
                if (res2.IsSuccessStatusCode)
                {
                    var result2 = res2.Content.ReadAsStringAsync().Result;
                    RespClientesModel Respuesta3 = JsonConvert.DeserializeObject<RespClientesModel>(result2);
                    Respuesta2 = Respuesta3.Data;
                }
            }
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(Respuesta2.ToPagedList(pageNumber, pageSize));
        }

        public async Task<IActionResult> ExportToExcel()
        {
            List<ClienteModel> Respuesta2 = new List<ClienteModel>();
            object user = new TokenModel();
            HttpClient client = _api.External();
            HttpResponseMessage res = await client.PostAsJsonAsync($"api/login/authenticate/", user);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                TokenModel Respuesta = JsonConvert.DeserializeObject<TokenModel>(result);
                client.DefaultRequestHeaders.Add("Authorization", Respuesta.Data);
                HttpResponseMessage res2 = await client.GetAsync($"api/customers");
                if (res2.IsSuccessStatusCode)
                {
                    var result2 = res2.Content.ReadAsStringAsync().Result;
                    RespClientesModel Respuesta3 = JsonConvert.DeserializeObject<RespClientesModel>(result2);
                    Respuesta2 = Respuesta3.Data;

                    using (var workbook = new XLWorkbook())
                    {
                        var worksheet = workbook.Worksheets.Add("Users");
                        var currentRow = 1;
                        worksheet.Cell(currentRow, 1).Value = "IdCliente";
                        worksheet.Cell(currentRow, 2).Value = "FechaRegistroEmpresa";
                        worksheet.Cell(currentRow, 3).Value = "RazonSocial";
                        worksheet.Cell(currentRow, 4).Value = "RFC";
                        worksheet.Cell(currentRow, 5).Value = "Sucursal";
                        worksheet.Cell(currentRow, 6).Value = "IdEmpleado";
                        worksheet.Cell(currentRow, 7).Value = "Nombre";
                        worksheet.Cell(currentRow, 8).Value = "Paterno";
                        worksheet.Cell(currentRow, 9).Value = "Materno";
                        worksheet.Cell(currentRow, 10).Value = "IdViaje";
                        foreach (var Respp in Respuesta2)
                        {
                            currentRow++;
                            worksheet.Cell(currentRow, 1).Value = Respp.IdCliente;
                            worksheet.Cell(currentRow, 2).Value = Respp.FechaRegistroEmpresa;
                            worksheet.Cell(currentRow, 3).Value = Respp.RazonSocial;
                            worksheet.Cell(currentRow, 4).Value = Respp.RFC;
                            worksheet.Cell(currentRow, 5).Value = Respp.Sucursal;
                            worksheet.Cell(currentRow, 6).Value = Respp.IdEmpleado;
                            worksheet.Cell(currentRow, 7).Value = Respp.Nombre;
                            worksheet.Cell(currentRow, 8).Value = Respp.Paterno;
                            worksheet.Cell(currentRow, 9).Value = Respp.Materno;
                            worksheet.Cell(currentRow, 10).Value = Respp.IdViaje;
                        }

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();

                            return File(
                                content,
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "clientes.xlsx");
                        }
                    }
                }
            }
            return null;
        }
    }
}
