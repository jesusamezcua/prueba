import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CRUDPersonaComponent } from './crudpersona.component';

describe('CRUDPersonaComponent', () => {
  let component: CRUDPersonaComponent;
  let fixture: ComponentFixture<CRUDPersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CRUDPersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CRUDPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
