﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBAPI.IServices;
using WEBAPI.Models;

namespace WEBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ILoginService _oLoginService;

        public LoginController(ILoginService oLoginService)
        {
            _oLoginService = oLoginService;
        }

        // PUT  Login
        [HttpPut]
        public Respuesta Get([FromBody] Login oLogin)
        {
            return _oLoginService.Get(oLogin);
        }

        // POST: Login
        [HttpPost]
        public Respuesta Post([FromBody] Login oLogin)
        {
            return _oLoginService.Save(oLogin);
        }

    }
}
