import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonaModel } from 'src/app/Modelos/Personas/persona-model';
import { Resp } from 'src/app/Modelos/Respuestas/resp';
import { PersonasServicioService } from 'src/app/Servicios/personas-servicio.service';

@Component({
  selector: 'app-crudpersona',
  templateUrl: './crudpersona.component.html',
  styleUrls: ['./crudpersona.component.css']
})
export class CRUDPersonaComponent implements OnInit {

  listaPersonas: PersonaModel[] = [];
  boolDatos: Boolean = false;
  res: Resp = new Resp;

  constructor(public dataservice: PersonasServicioService, public router: Router) { }

  ngOnInit() {
    this.cargarPersonas();
  }

  async cargarPersonas() {
    try {
      this.listaPersonas = await this.dataservice.getPersonas();
    if (this.listaPersonas.length > 0) {
      this.boolDatos = true;
    }
    else {
      this.boolDatos = false;
    }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  async EliminarPersona(idPersonaFisica: number) {
    try {
      if (confirm("Estas seguro que deseas eliminar esta persona?")) {
        this.res = await this.dataservice.deletePersona(idPersonaFisica);
        if(this.res.intStatus == 200){
          alert("Se elimino la persona !!!");
          this.cargarPersonas();
        }else{
          alert("Ocurrio un error !!!");
        }
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  CerrarSesion(){
    delete localStorage.token;
    this.router.navigateByUrl('/login');
  }

}
