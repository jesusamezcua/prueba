﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontWeb.Models
{
    public class RespPersonaModel
    {
        public int intStatus { get; set; }
        public string vchMensaje { get; set; }
        public int IdPersona { get; set; }
    }
}
