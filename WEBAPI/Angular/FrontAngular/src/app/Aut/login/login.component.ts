import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/Servicios/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Correo: string;
  Contrasena: string;
  
  constructor(public userService: UsuarioService, public router: Router) { }

  ngOnInit() {
  }

  async login() {
    try {
      const user = {Correo: this.Correo, Contrasena: this.Contrasena};
      let res = await this.userService.login(user);
      if(res['intStatus'] == 200){
        this.userService.setToken(res['idUsuario']);//EN lugar del token solo guardo el id del usuario para no modificar el SP pero lo correcto seria editar el SP para que nos regrese un token
        this.router.navigateByUrl('/');
      }else{
        alert("Ocurrio un error !!! " + res['vchMensaje']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }
}
