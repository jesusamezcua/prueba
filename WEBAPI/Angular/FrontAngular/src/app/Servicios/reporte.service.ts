import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClientesModel } from '../Modelos/Personas/clientes-model';
import { TokenModel } from '../Modelos/Personas/token-model';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  readonly strUrl = environment.strUrlReporte;
  employees: Observable<ClientesModel[]> | undefined;
  constructor(private http: HttpClient) {
  }

  public getToken(): Promise<TokenModel> {
    let user: TokenModel = new TokenModel();
    console.log(user);
    return this.http.post<TokenModel>(this.strUrl+'login/authenticate/', user).toPromise();
  }

  public getClientes(data): Promise<ClientesModel[]> {
    var header = {
      headers: new HttpHeaders()
        .set('Authorization',  data)
    }
    return this.http.get<ClientesModel[]>(this.strUrl+'/customers',header).toPromise();
  }
}
