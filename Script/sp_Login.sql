IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_Login' ) 
		DROP PROCEDURE sp_Login
GO

CREATE PROCEDURE sp_Login
(
    @Correo VARCHAR(50),
    @Contrasena VARCHAR(50)
)
AS
BEGIN
    SET NOCOUNT ON;
	DECLARE @Respuesta AS TABLE (
		intStatus INT,
		vchMensaje VARCHAR(500),
		IdUsuario INT
	)
    BEGIN TRY
        IF NOT EXISTS
        (
            SELECT *
            FROM dbo.Tb_Usuarios
            WHERE Correo = @Correo
                  AND Contrasena = @Contrasena
        )
        BEGIN
			INSERT INTO @Respuesta (
				intStatus, vchMensaje, IdUsuario
			) VALUES (
				-50000, 'Correo o contrasena incorrecto', -1
			)
        END;
		ELSE
			INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdUsuario
				) SELECT
					200, 
					'Inicio exitoso', 
					U.IdUsuario
			FROM dbo.Tb_Usuarios U
            WHERE Correo = @Correo
                  AND Contrasena = @Contrasena
    END TRY
    BEGIN CATCH
        PRINT ERROR_MESSAGE();
		INSERT INTO @Respuesta (
				intStatus, vchMensaje, IdUsuario
			) VALUES (
				-50000, 'Error al iniciar sesion.', -1
			)
    END CATCH;
END;
SELECT intStatus, vchMensaje, IdUsuario
  FROM @Respuesta
GO