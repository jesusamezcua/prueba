IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_AgregarPersonaFisica' ) 
		DROP PROCEDURE sp_AgregarPersonaFisica
GO
CREATE PROCEDURE dbo.sp_AgregarPersonaFisica
(
    @Nombre VARCHAR(50),
    @ApellidoPaterno VARCHAR(50),
    @ApellidoMaterno VARCHAR(50),
    @RFC VARCHAR(13),
    @FechaNacimiento DATE,
    @UsuarioAgrega INT
)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Respuesta AS TABLE (
		intStatus INT,
		vchMensaje VARCHAR(500),
		IdPersona INT
	)
    BEGIN TRY
        IF LEN(@RFC) = 13
        BEGIN
			IF EXISTS
			(
				SELECT *
				FROM dbo.Tb_PersonasFisicas
				WHERE RFC = @RFC
					  AND Activo = 1
			)
			BEGIN
				INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					-50000, 'El RFC ya existe en el sistema', -1
				)
			END;
			ELSE
			BEGIN
				INSERT INTO dbo.Tb_PersonasFisicas
				(
					FechaRegistro,
					FechaActualizacion,
					Nombre,
					ApellidoPaterno,
					ApellidoMaterno,
					RFC,
					FechaNacimiento,
					UsuarioAgrega,
					Activo
				)
				VALUES
				(   GETDATE(),        -- FechaRegistro - datetime
					NULL,             -- FechaActualizacion - datetime
					@Nombre,          -- Nombre - varchar(50)
					@ApellidoPaterno, -- ApellidoPaterno - varchar(50)
					@ApellidoMaterno, -- ApellidoMaterno - varchar(50)
					@RFC,             -- RFC - varchar(13)
					@FechaNacimiento, -- FechaNacimiento - date
					@UsuarioAgrega,   -- UsuarioAgrega - int
					1                 -- Activo - bit
					);
				INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					200, 'Registro exitos', SCOPE_IDENTITY()
				)
			END;
		END;
		ELSE
		BEGIN
			INSERT INTO @Respuesta (
				intStatus, vchMensaje, IdPersona
			) VALUES (
				-50000, 'RFC NO VALIDO', -1
			)
        END;
    END TRY
    BEGIN CATCH
        PRINT ERROR_MESSAGE();
		INSERT INTO @Respuesta (
				intStatus, vchMensaje, IdPersona
			) VALUES (
				-50000, 'Error al guardar el registro.', -1
			)
    END CATCH;
END;

SELECT intStatus, vchMensaje, IdPersona
  FROM @Respuesta
GO