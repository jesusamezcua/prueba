﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WEBAPI.Common;
using WEBAPI.IServices;
using WEBAPI.Models;

namespace WEBAPI.Services
{
    public class LoginService : ILoginService
    {
        Respuesta _oResp = new Respuesta();

        //Logear Usuario
        public Respuesta Get(Login oLogin)
        {
            _oResp = new Respuesta();
            try
            {
                using (IDbConnection con = new SqlConnection(Global.ConnectionString))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var oResp = con.Query<Respuesta>("sp_Login",
                        this.SetParameters(oLogin),
                        commandType: CommandType.StoredProcedure);
                    _oResp = oResp.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _oResp.vchMensaje = ex.Message;
            }
            return _oResp;
        }

        //Registrar usuario
        public Respuesta Save(Login oLogin)
        {
            _oResp = new Respuesta();
            try
            {
                using (IDbConnection con = new SqlConnection(Global.ConnectionString))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var oResp = con.Query<Respuesta>("sp_AgregarUsuario",
                        this.SetParameters(oLogin),
                        commandType: CommandType.StoredProcedure);
                    _oResp = oResp.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _oResp.vchMensaje = ex.Message;
            }
            return _oResp;
        }

        private DynamicParameters SetParameters(Login oLogin)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Correo", oLogin.Correo);
            parameters.Add("@Contrasena", oLogin.Contrasena);
            return parameters;
        }
    }
}
