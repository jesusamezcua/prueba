﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBAPI.Models;

namespace WEBAPI.IServices
{
    public interface IPersonaService
    {
        Resp Save(Persona oPersona);
        Resp Update(Persona oPersona, int id);
        List<Persona> Gets();
        Persona Get(int IdPersonaFisica);
        Resp Delete(int IdPersonaFisica);

    }
}
