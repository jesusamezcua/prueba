export class ClientesModel {
    IdCliente:number = 0;
    FechaRegistroEmpresa:Date= new Date();
    RazonSocial:string = '';
    RFC:string = '';
    Sucursal:string = '';
    IdEmpleado: number = 0;
    Nombre:string = '';
    Paterno:string = '';
    Materno:string = '';
    IdViaje:number = 0;
}
