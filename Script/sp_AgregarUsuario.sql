IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_AgregarUsuario' ) 
		DROP PROCEDURE sp_AgregarUsuario
GO

CREATE PROCEDURE sp_AgregarUsuario
(
    @Correo VARCHAR(50),
    @Contrasena VARCHAR(50)
)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Respuesta AS TABLE (
		intStatus INT,
		vchMensaje VARCHAR(500),
		IdUsuario INT
	)
    BEGIN TRY
        IF EXISTS
        (
            SELECT *
            FROM dbo.Tb_Usuarios
            WHERE Correo = @Correo
        )
        BEGIN
			INSERT INTO @Respuesta (
				intStatus, vchMensaje, IdUsuario
			) VALUES (
				-50000, 'El CORREO ya existe en el sistema', -1
			)
        END;
		ELSE
		BEGIN
			INSERT INTO dbo.Tb_Usuarios
			(
				FechaRegistro,
				Correo,
				Contrasena
			)
			VALUES
			(   GETDATE(),           -- FechaActualizacion - datetime
				@Correo, 
				@Contrasena
				);
			INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdUsuario
				) VALUES (
					200, 
					'Registro exitoso', 
					SCOPE_IDENTITY())
		END;
    END TRY
    BEGIN CATCH
        PRINT ERROR_MESSAGE();
		INSERT INTO @Respuesta (
				intStatus, vchMensaje, IdUsuario
			) VALUES (
				-50000, 'Error al guardar el registro.', -1
			)
    END CATCH;
END;
SELECT intStatus, vchMensaje, IdUsuario
  FROM @Respuesta
GO