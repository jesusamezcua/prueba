import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../Modelos/Usuario/usuario';
import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  readonly strUrl = environment.strUrl;
  
  constructor(private http: HttpClient, private cookies: CookieService) { }

  login(user: Usuario) {
    return this.http.put(this.strUrl+'/Login', user).toPromise();
  }

  register(user: Usuario) {
    return this.http.post(this.strUrl+'/Login', user).toPromise();
  }

  setToken(token) {
    localStorage.setItem("token", token);
  }
  getToken() {
    return localStorage.getItem("token");
  }
}
