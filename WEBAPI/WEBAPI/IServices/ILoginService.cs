﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBAPI.Models;

namespace WEBAPI.IServices
{
    public interface ILoginService
    {
        Respuesta Save(Login oLogin);
        Respuesta Get(Login oLogin);
    }
}
