﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBAPI.Models
{
    public class Respuesta
    {
        public int intStatus { get; set; }
        public string vchMensaje { get; set; }
        public int IdUsuario { get; set; }
    }
}
