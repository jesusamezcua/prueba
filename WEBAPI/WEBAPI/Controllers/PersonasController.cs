using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBAPI.IServices;
using WEBAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : ControllerBase
    {
        private IPersonaService _oPersonaService;

        public PersonasController(IPersonaService oPersonaService)
        {
            _oPersonaService = oPersonaService;
        }

        // GET: api/<PersonasController>
        [HttpGet]
        public IEnumerable<Persona> Get()
        {
            return _oPersonaService.Gets();
        }

        // GET api/<PersonasController>/5
        [HttpGet("{id}")]
        public Persona Get(int id)
        {
            return _oPersonaService.Get(id);
        }

        // POST api/<PersonasController>
        [HttpPost]
        public Resp Post([FromBody] Persona oPersona)
        {
            return _oPersonaService.Save(oPersona);
        }

        // PUT api/<PersonasController>/5
        [HttpPut("{id}")]
        public Resp Put(int id, [FromBody] Persona oPersona)
        {
            if (ModelState.IsValid) return _oPersonaService.Update(oPersona, id);
            return null;
        }

        // DELETE api/<PersonasController>/5
        [HttpDelete("{id}")]
        public Resp Delete(int id)
        {
            if (ModelState.IsValid)  return _oPersonaService.Delete(id);
            return null;
        }
    }
}
