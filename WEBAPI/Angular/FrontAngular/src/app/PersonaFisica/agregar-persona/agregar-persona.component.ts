import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonaModel } from 'src/app/Modelos/Personas/persona-model';
import { PersonasServicioService } from 'src/app/Servicios/personas-servicio.service';
import { Resp } from 'src/app/Modelos/Respuestas/resp';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.component.html',
  styleUrls: ['./agregar-persona.component.css']
})
export class AgregarPersonaComponent implements OnInit {
  res: Resp = new Resp;
  private suscribe: Subscription;
  idPersonaFisica: number;
  boolEditar: boolean = false;

  constructor(private dataservice: PersonasServicioService,  private routerService: Router, private routeService: ActivatedRoute) {
    this.suscribe = new Subscription();
  }
  RegistrarPersonaForm = new FormGroup({
    Nombre: new FormControl('', [Validators.required]),
    ApellidoPaterno: new FormControl('', [ Validators.required]),
    ApellidoMaterno: new FormControl('', [ Validators.required]),
    RFC: new FormControl('', [Validators.required, Validators.minLength(13)]),
    FechaNacimiento: new FormControl( [Validators.required]),
    UsuarioAgrega: new FormControl( [Validators.required])
  });

  async ngOnInit() {
    await this.suscribe.add(
      this.routeService.paramMap.subscribe(params => {
        if (params.get('idPersonaFisica')) {
          this.idPersonaFisica =  parseInt(params.get('idPersonaFisica'));
          this.Cargar();
          this.boolEditar = true;
        }
      }))
  }

  async Cargar(){
    try {
      let Persona = await this.dataservice.getPersona(this.idPersonaFisica);
      this.RegistrarPersonaForm.controls['Nombre'].setValue(Persona['nombre']);
      this.RegistrarPersonaForm.controls['ApellidoPaterno'].setValue(Persona['apellidoPaterno']);
      this.RegistrarPersonaForm.controls['ApellidoMaterno'].setValue(Persona['apellidoMaterno']);
      this.RegistrarPersonaForm.controls['RFC'].setValue(Persona['rfc']);
      this.RegistrarPersonaForm.controls['FechaNacimiento'].setValue(Persona['fechaNacimiento']);
      this.RegistrarPersonaForm.controls['UsuarioAgrega'].setValue(Persona['usuarioAgrega']);
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  async Guardar(){
    try {
      let Persona: PersonaModel = new PersonaModel(this.RegistrarPersonaForm.value);
      this.res = await this.dataservice.agregarPersona(Persona);
      if(this.res.intStatus == 200){
        alert("Se agrego la persona !!!");
        this.routerService.navigateByUrl("");
      }else{
        alert("Ocurrio un error !!!");
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }

  async Editar(){
    try {
      let Persona: PersonaModel = new PersonaModel(this.RegistrarPersonaForm.value);
      this.res = await this.dataservice.modificarPersona(Persona, this.idPersonaFisica);
      if(this.res.intStatus == 200){
        alert("Se Mofifico la persona !!!");
        this.routerService.navigateByUrl("");
      }else{
        alert("Ocurrio un error !!!");
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }
}
