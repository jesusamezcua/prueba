IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_ActualizarPersonaFisica' ) 
		DROP PROCEDURE sp_ActualizarPersonaFisica
GO

CREATE PROCEDURE dbo.sp_ActualizarPersonaFisica
(
    @IdPersonaFisica INT,
    @Nombre VARCHAR(50),
    @ApellidoPaterno VARCHAR(50),
    @ApellidoMaterno VARCHAR(50),
    @RFC VARCHAR(13),
    @FechaNacimiento DATE,
    @UsuarioAgrega INT
)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Respuesta AS TABLE (
		intStatus INT,
		vchMensaje VARCHAR(500),
		IdPersona INT
	)
    BEGIN TRY
        IF NOT EXISTS
        (
            SELECT *
            FROM dbo.Tb_PersonasFisicas
            WHERE IdPersonaFisica = @IdPersonaFisica
                  AND Activo = 1
        )
        BEGIN
			INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					-50000, 'La persona fisica no existe.', -1
				)
        END;
		ELSE
		BEGIN
			UPDATE dbo.Tb_PersonasFisicas
			SET Nombre = @Nombre,
				ApellidoPaterno = @ApellidoPaterno,
				ApellidoMaterno = @ApellidoMaterno,
				RFC = @RFC,
				FechaNacimiento = @FechaNacimiento
			WHERE IdPersonaFisica = @IdPersonaFisica;
			INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					200, 'La persona fisica MODIFICADA.', -1
				)
		END;
    END TRY
    BEGIN CATCH
        PRINT ERROR_MESSAGE();
		INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					-50000, 'Error al actualizar el registro.', -1
				)
    END CATCH;
END;

SELECT intStatus, vchMensaje, IdPersona
  FROM @Respuesta
GO