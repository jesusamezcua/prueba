import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/Servicios/usuario.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  email: string;
  password: string;
  confirmPassword: string;

  constructor(public userService: UsuarioService, public router: Router) { }

  ngOnInit() {
  }

  async register() {
    try {
      const user = {Correo: this.email, Contrasena: this.password};
      let res = await this.userService.register(user);
      if(res['intStatus'] == 200){
        this.userService.setToken(res['IdUsuario']);//EN lugar del token solo guardo el id del usuario para no modificar el SP pero lo correcto seria editar el SP para que nos regrese un token
      this.router.navigateByUrl('/');
      }else{
        alert("Ocurrio un error !!! " + res['vchMensaje']);
      }
    } catch (error) {
      alert("Error en el servidor !!!");
    }
  }
}