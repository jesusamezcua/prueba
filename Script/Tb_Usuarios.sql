CREATE TABLE Tb_Usuarios
(
    IdUsuario INT IDENTITY,
	Correo VARCHAR(50),
    FechaRegistro DATETIME,
    Contrasena VARCHAR(50)
);

ALTER TABLE Tb_Usuarios
ADD CONSTRAINT [PK_Tb_Usuarios]
    PRIMARY KEY (IdUsuario);
