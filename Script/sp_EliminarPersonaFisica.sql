
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_EliminarPersonaFisica' ) 
		DROP PROCEDURE sp_EliminarPersonaFisica
GO
CREATE PROCEDURE dbo.sp_EliminarPersonaFisica
(@IdPersonaFisica INT)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Respuesta AS TABLE (
		intStatus INT,
		vchMensaje VARCHAR(500),
		IdPersona INT
	)
    BEGIN TRY
        IF NOT EXISTS
        (
            SELECT *
            FROM dbo.Tb_PersonasFisicas
            WHERE IdPersonaFisica = @IdPersonaFisica
                  AND Activo = 1
        )
        BEGIN
			INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					-50000, 'La persona fisica no existe.', -1
				)
        END;
		ELSE
		BEGIN
			UPDATE dbo.Tb_PersonasFisicas
			SET Activo = 0
			WHERE IdPersonaFisica = @IdPersonaFisica;
			INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					200, 'La persona fisica eliminada.', -1
				)
		END;
    END TRY
    BEGIN CATCH
        PRINT ERROR_MESSAGE();
		INSERT INTO @Respuesta (
					intStatus, vchMensaje, IdPersona
				) VALUES (
					-50000, 'Error al eliminar el registro.', -1
				)
    END CATCH;
END;

SELECT intStatus, vchMensaje, IdPersona
  FROM @Respuesta
GO